﻿using Nancy;
using Nancy.Authentication.Token;
using Nancy.Conventions;
using Nancy.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hackalong
{
    public class HackalongBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(Nancy.TinyIoc.TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines)
        {
            // Enable Cors
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {  
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                                .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type, Access-Control-Allow-Headers")
                                .WithHeader("Access-Control-Expose-Headers", "GET");

            });

            base.ApplicationStartup(container, pipelines);
        }

        protected override void RequestStartup(Nancy.TinyIoc.TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines, NancyContext context)
        {
            // TokenAuthentication.Enable(pipelines, new TokenAuthenticationConfiguration(container.Resolve<ITokenizer>()));
            base.RequestStartup(container, pipelines, context);
        }
        
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            nancyConventions.StaticContentsConventions.Add(
                        StaticContentConventionBuilder.AddDirectory("ui", @"bin/ui"));

            nancyConventions.StaticContentsConventions.Add(
                     StaticContentConventionBuilder.AddDirectory("fonts", @"bin/ui/fonts"));

            nancyConventions.StaticContentsConventions.Add(
                        StaticContentConventionBuilder.AddDirectory("images", @"bin/ui/images"));
         
            nancyConventions.StaticContentsConventions.Add(
                        StaticContentConventionBuilder.AddDirectory("scripts", @"bin/ui/scripts"));

            nancyConventions.StaticContentsConventions.Add(
                     StaticContentConventionBuilder.AddDirectory("styles", @"bin/ui/styles"));

            nancyConventions.StaticContentsConventions.Add(
                     StaticContentConventionBuilder.AddDirectory("views", @"bin/ui/views"));

            base.ConfigureConventions(nancyConventions);
        }
    }
}