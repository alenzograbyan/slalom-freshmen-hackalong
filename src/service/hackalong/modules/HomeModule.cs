﻿using Nancy;
using Nancy.Authentication.Token;
using Nancy.Cryptography;
using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hackalong.modules
{
    public class HomeModule : NancyModule
    {
        private const int SessionExpirationTimeInSeconds = 30;
        private const string SecretKey = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
        private const string TokenKey = "Bearer ";

        public HomeModule()
        {
            Get["/"] = (parameters) =>
                {
                    return Response.AsFile(@"bin/ui/index.html", "text/html");
                };

            Get["/api/usersession"] = (parameters) =>
                {
                    string token;
                    if (!HasValidToken(Context.Request.Headers, out token))
                    {
                        token = GenerateNewToken(Guid.NewGuid().ToString());
                    }

                    return Response.AsJson(new { token = token })
                        .WithHeader("Authorization", token);
                };
        }

        private static string GenerateNewToken(string userId)
        {
            var expirationDate = DateTime.UtcNow;
            var now = Math.Round((expirationDate - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);

            TimeZoneInfo caTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            TimeSpan offset = caTimeZoneInfo.BaseUtcOffset;

            if (caTimeZoneInfo.IsDaylightSavingTime(expirationDate))
            {
                offset = offset.Add(new TimeSpan(1, 0, 0));
            }            

            var payload = new Dictionary<string, object>() 
            {
                { "SlalomFreshmentUserId", userId },
                { "exp", now },
                {"formattedExpiration", expirationDate.Add(offset).ToString()}
            };

            return JWT.JsonWebToken.Encode(payload, HomeModule.SecretKey, JWT.JwtHashAlgorithm.HS256);
        }

        private static bool HasValidToken(RequestHeaders requestHeaders, out string token)
        {
            bool isValid = false;
            token = null;



            try
            {
                token = requestHeaders.Authorization.Substring(TokenKey.Length);
                var payload = JWT.JsonWebToken.DecodeToObject(token, HomeModule.SecretKey) as IDictionary<string, object>;

                int expSeconds = (int)payload["exp"];

                if (Math.Round((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds)
                    <= expSeconds + HomeModule.SessionExpirationTimeInSeconds)
                {
                    isValid = true;
                }

            }
            catch
            {
                // Do nothing.
            }


            return isValid;
        }

        private UserSessionContainer CreateUserToken()
        {
            UserSessionContainer newSession = new UserSessionContainer();
            Session[newSession.UserId.ToString()] = newSession;

            return newSession;
        }

        [Serializable]
        private class UserSessionContainer
        {
            public UserSessionContainer()
            {
                CreatedDate = DateTimeOffset.Now;
                UserId = Guid.NewGuid();
            }

            public Guid UserId { get; private set; }

            public DateTimeOffset CreatedDate { get; private set; }
        }
    }
}
