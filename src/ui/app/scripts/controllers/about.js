'use strict';

/**
 * @ngdoc function
 * @name hackalongApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hackalongApp
 */
angular.module('hackalongApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.testing = {};
  });
